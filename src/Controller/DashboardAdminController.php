<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardAdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }
    /**
     * @Route("/admin/complete/tricks", name="admin_complete_trick")
     */
    public function trickAdmin()
    {
        return $this->render('admin/admin-tricks.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }
    /**
     * @Route("/admin/members", name="admin_members")
     */
    public function membersAdmin()
    {
        return $this->render('admin/admin-members.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }

    /**
     * @Route("/admin/comments", name="admin_comments")
     */
    public function commentsAdmin()
    {
        return $this->render('admin/admin-comments.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }

}
