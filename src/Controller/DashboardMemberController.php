<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DashboardMemberController extends AbstractController
{
    /**
     * @Route("/dashboard/member", name="dashboard")
     */
    public function index()
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        return $this->render('dashboard_member/index.html.twig', [
            'controller_name' => 'DashboardMemberController',
        ]);
    }
    /**
     * @Route("/dashboard/member-name/tricks", name="dashboard_member_trick")
     */
    public function tricks()
    {
        return $this->render('dashboard_member/tricks.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }
    /**
     * @Route("/dashboard/member-name/profil", name="dashboard_member_profil")
     */
    public function profil()
    {
        return $this->render('dashboard_member/profil.html.twig', [
            'controller_name' => 'DashboardAdminController',
        ]);
    }
}
