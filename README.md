
# ssj-tricks

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/2bbd815c3d0c4661a21ee77ba97a2274)](https://app.codacy.com/manual/samakunchan/ssj-tricks?utm_source=github.com&utm_medium=referral&utm_content=samakunchan/ssj-tricks&utm_campaign=Badge_Grade_Dashboard)
[![PHP Badge](https://img.shields.io/badge/Language-PHP-blueviolet)](https://www.php.net/docs.php)
[![Techno Badge](https://img.shields.io/badge/Technology-Symfony5-blue)](https://www.php.net/docs.php)
[![Composer Badge](https://img.shields.io/badge/Dependency-Composer-lightgrey)](https://www.php.net/docs.php)

## Projet 6 OpenClassroom- Snowboard Sweat Jimmy Tricks alias SSJ Tricks.

![center](https://www.adventureconseil.com/wp-content/uploads/2019/01/openclassrooms.jpg)
![center](http://www.diapason-info.com/wp-content/uploads/2014/07/symfony_logo.png)
